#! /usr/bin/env make -f

SHELL = /bin/sh

INSTALL = /usr/bin/install
INSTALL_FILES = ${INSTALL} -vDm 644

DESTDIR ?=

prefix = /usr
sysconfdir = /etc
libdir = ${prefix}/lib
datarootdir = ${prefix}/share

.PHONY: help
help:
	@echo 'TARGETS:'
	@echo '  help __________________ show this help message'
	@echo '  install _______________ install the project'
	@echo ''
	@echo 'OPTIONS:'
	@echo '  DESTDIR _______________ directory for staged installs [default: (none)]'
	@echo '  prefix ________________ install prefix [default: "/usr"] '
	@echo '  libdir ________________ static data directory prefix [default: "$${prefix}/lib"]'
	@echo '  datarootdir ___________ read-only program data prefix [default: "$${prefix}/share"]'

.PHONY: install
install:
	@echo "INSTALLING"

	@echo 'installing default config file'
	$(INSTALL_FILES) -T dist/server.conf \
		${DESTDIR}${datarootdir}/factory/etc/kmap/gis/server.conf

	@echo 'installing nginx configuration'
	$(INSTALL_FILES) -t ${DESTDIR}${sysconfdir}/nginx/sites-available/ \
		dist/nginx/gis-server.conf

	@echo 'installing systemd units'
	$(INSTALL_FILES) -t ${DESTDIR}${libdir}/systemd/system/ \
		dist/systemd/gis-server@.socket \
		dist/systemd/gis-server@.service

	@echo 'installing systemd presets'
	$(INSTALL_FILES) -t ${DESTDIR}${libdir}/systemd/system-preset/ \
		dist/systemd/78-gis-server.preset

	@echo 'allocating sysusers'
	$(INSTALL_FILES) -T dist/gisserver.sysusers \
		${DESTDIR}${libdir}/sysusers.d/gisserver.conf

	@echo 'configuring tmpfiles'
	$(INSTALL_FILES) -T dist/gisserver.tmpfiles \
		${DESTDIR}${libdir}/tmpfiles.d/gisserver.conf

	@echo "ok"
